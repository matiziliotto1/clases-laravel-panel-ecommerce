## Clonar proyecto

1-git clone "http del repositorio"
2-Navegar a la carpeta donde se clono y ejecutar los comandos:
    i-      "composer install"
    ii-     "npm install"
    iii-    "npm run dev"
    iv-     "cp .env.example .env"  -> Copia el archivo .env.example en un nuevo archivo llamado .env. 
                En el archivo .env configurar el nombre de base de datos.
    v-      "php artisan key:generate"
3-Setup de base de datos:
    i-      "php artisan migrate"
4-Correr proyecto:
    i-      "php artisan serve"