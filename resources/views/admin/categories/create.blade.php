@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            Crear categoria
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{route('admin.categories.store')}}" method="POST">
                            @csrf
                            @include('admin.categories.fields')

                            <div class="float-right">
                                <a href="{{ route('admin.categories.index') }}" class="btn">Cancelar</a>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection