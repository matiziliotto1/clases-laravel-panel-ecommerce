<input type="hidden" name="id" id="id" value="{{ $category->id ?? old('id') }} ">

<div class="form-group">
    <label for="name">Nombre</label>
    <input type="text" name="name" id="name" value="{{ $category->name ?? old('name') }}"
        autofocus class="form-control @error('name') is-invalid @enderror">
    @error('name')
        <div class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </div>
    @enderror
</div>