@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            Categorias
                        </div>
                        <div class="float-right">
                            <a href="{{route('admin.categories.create')}}">Crear categoria</a>
                        </div>
                    </div>
                    @include('layouts.alerts.success')
                    @include('layouts.alerts.warning')
                    @include('layouts.alerts.danger')
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($categories ?? [] as $category)
                                    <tr>
                                        <td>{{$category->id}}</td>
                                        <td>{{$category->name}}</td>
                                        <td>
                                            <a href="{{ route('admin.categories.edit', $category->id) }}">
                                                <span class="badge badge-primary">Editar</span>
                                            </a>
                                            <a href="{{ route('admin.categories.destroy', $category) }}" onclick="event.preventDefault();document.getElementById('delete-category').submit();">
                                                <span class="badge badge-danger">Eliminar</span>
                                            </a>
                                            <form id="delete-category" action="{{ route('admin.categories.destroy', $category) }}" method="POST" class="d-none">
                                                @csrf
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="3">
                                            <p>
                                                Aún no hay categorias creadas
                                            </p>
                                        </td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection